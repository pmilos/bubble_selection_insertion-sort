#include "data.h"

void Insertion_sort(int* tablica, int rozmiar, int suma)
{
	int i, j = 0;
	int temp = 0;
	DWORD timer = GetTickCount();

	for (i = 1; i <= rozmiar - 1; i++) {
		j = i;

		while (j > 0 && tablica[j] < tablica[j - 1]) {
			temp = tablica[j];
			tablica[j] = tablica[j - 1];
			tablica[j - 1] = temp;

			j--; suma++;
		}
	}
	timer = GetTickCount() - timer;

	for (int i = 0; i < rozmiar; i++)
	{
		printf("%i \t %i \n", tablica[i], i + 1);
	}
	printf("Liczba przesuniec: %i\n", suma);
	printf("Czas obliczen wynosi: %i\n", timer);
}



int main()
{
	int* tablica = 0;
	int rozmiar = 0;
	int suma = 0;

	printf("* Insertion sort *\n");
	printf("Podaj rozmiar tablicy: ");
	scanf_s("%i", &rozmiar);

	if ((tablica = (int*)calloc(rozmiar, sizeof(int))) == 0)
	{
		printf("Nie mozna zarezerwowac pamieci");
		system("pause");
		return EXIT_FAILURE;
	}

	srand(time(NULL));

	for (int i = 0; i < rozmiar; i++)
	{
		tablica[i] = rand();
	}
	printf("\n");

	Insertion_sort(tablica, rozmiar, suma);
	
	
	system("pause");
	return EXIT_SUCCESS;
}