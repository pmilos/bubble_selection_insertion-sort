#include "data.h"


void Bubble_sort(int* array, int size, int count)
{
	int i, j = 0;
	int temp = 0;
	DWORD timer = GetTickCount();
	for (i = 0; i < size; i++)
	{
		for (j = 0; j < size - i; j++)
		{
			if (array[j]<array[j - 1])
			{
				
				temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
				count++;
			}
		}
	}
	timer = GetTickCount() - timer;

	for (int i = 0; i < size; i++)
	{
		printf("%i \t %i \n", array[i], i + 1);
	}
	printf("Liczba przesuniec: %i\n", count);
	printf("Czas obliczen wynosi: %i\n", timer);
}

int main()
{
	int* random = 0;
	int size = 0;
	int count = 0;

	printf("* Bubble sort *\n");
	printf("Podaj rozmiar tablicy: ");
	scanf_s("%i", &size);

	if ((random = (int*)calloc(size, sizeof(int))) == 0)
	{
		printf("Nie mozna zarezerwowac pamieci");
		system("pause");
		return EXIT_FAILURE;
	}

	srand(time(NULL));

	for (int i = 0; i < size; i++)
	{
		random[i] = rand();
	}
	printf("\n");

	Bubble_sort(random, size, count);
	
	printf("\n");

	system("PAUSE");
	return EXIT_FAILURE;
}