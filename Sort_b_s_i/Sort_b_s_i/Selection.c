#include "data.h"



void Selection_sort(int* tablica, int rozmiar, int suma)
{
	int pozycja = 0, zamien = 0;
	DWORD timer = GetTickCount();

	for (size_t i = 0; i < (rozmiar - 1); i++)
	{
		pozycja = i;

		for (size_t j = i + 1; j < rozmiar; j++)
		{
			if (tablica[pozycja] > tablica[j])
			{
				pozycja = j;
			}
		}

		if (pozycja != i)
		{
			zamien = tablica[i];
			tablica[i] = tablica[pozycja];
			tablica[pozycja] = zamien;
			suma++;
		}
	}
	timer = GetTickCount() - timer;

	for (size_t i = 0; i < rozmiar; i++)
	{
		printf("%i \t %i \n", tablica[i], i + 1);
	}
	puts("");

	printf("Ilosc przesuniec: %i\n", suma);
	printf("Czas obliczen wynosi: %i\n", timer);
}


int main()
{
	int* tablica = 0;
	int rozmiar = 0;
	int suma = 0;

	printf("* Selection sort *\n");
	printf("Podaj rozmiar tablicy: ");
	scanf_s("%i", &rozmiar);

	if ((tablica = (int*)calloc(rozmiar, sizeof(int))) == 0)
	{
		printf("Nie mozna zarezerwowac pamieci");
		system("pause");
		return EXIT_FAILURE;
	}

	srand(time(NULL));

	for (int i = 0; i < rozmiar; i++)
	{
		tablica[i] = rand();
	}
	printf("\n");

	Selection_sort(tablica, rozmiar, suma);


	system("pause");
	return EXIT_SUCCESS;
}